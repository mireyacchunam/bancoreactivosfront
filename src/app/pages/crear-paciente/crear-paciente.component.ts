import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PacientesService } from '../../services/pacientes.service';
import Swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-crear-paciente',
  templateUrl: './crear-paciente.component.html',
  styleUrls: ['./crear-paciente.component.css']
})
export class CrearPacienteComponent{

  pacientes: any = {};
  selectedFile: any = '';

//url; //Angular 8
url: any; //Angular 11, for stricter type
msg = "";



  constructor(private pacientesServices: PacientesService, private router: Router, private http:HttpClient) { }

  AltaPaciente(){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Estás seguro de almacenar?',
      text: "Puedes cancelar!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, agregar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.pacientesServices.altaPaciente(this.pacientes).subscribe( (resp:any) => {
          if (resp['resultado'] == 'OK') {
            swalWithBootstrapButtons.fire(
              'Registro Agregado!',
              'Tu registro ha sido agregado con exito.',
              'success'
            )
           this.router.navigate(['/nuevo-historial']);
          }
        });
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancel',
          'No se ha registrado!)',
          'error'
        )
      }
    })

    /*console.log(this.pacientes);
    this.pacientesServices.altaPaciente(this.pacientes).subscribe( (resp:any) => {
      if (resp['resultado'] == 'OK') {
        console.log('Registro exitoso');
        this.router.navigate(['/nuevo-historial']);
      }

    });*/

  }

  onFileChanged(event:any){
    if(!event.target.files[0] || event.target.files[0].length == 0) {
      this.msg = 'Debes seleccionar una imagen';
      return;
    }
    
    var mimeType = event.target.files[0].type;
    
    if (mimeType.match(/image\/*/) == null) {
      this.msg = "Solo imagenes";
      return;
    }
    
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    
    reader.onload = (_event) => {
      this.msg = "";
      this.url = reader.result; 
    }

    this.selectedFile = event.target.files[0];
    console.log(event);
  }
  

  onUpload(){
    const uploadData = new FormData();
    uploadData.append('myFile', this.selectedFile, this.selectedFile.name);
    this.http.post('http://localhost/Mireyitaa/banco-reactivos/banco-reactivos/medico/Upload.php', uploadData, {
      reportProgress: true,
      observe: 'events'
    })
    .subscribe(event => {
      console.log(event);
    });
  }


}
