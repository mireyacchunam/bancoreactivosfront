import { Component, OnInit } from '@angular/core';
import { PacientesService } from '../../services/pacientes.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Pacientes } from '../../interfaces/pacientes.interface';

@Component({
  selector: 'app-nuevo-historial',
  templateUrl: './nuevo-historial.component.html',
  styleUrls: ['./nuevo-historial.component.css']
})
export class NuevoHistorialComponent implements OnInit {

  mostrar = false;
  //pacientes: any = {};
  pacientes: Pacientes[] | any;
  //fecha = new Date().getDate() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getFullYear();

  newhistorial: any = {
    //citahistorial: this.fecha
  };

  constructor(public pacienteService: PacientesService, private route: Router) {
    //console.log(this.fecha);
   }

  ngOnInit(): void {
    this.obtenerPacientes();
  }

  obtenerPacientes() {
    this.pacienteService.getPaciente().subscribe( (resp: any) => {
      this.pacientes = resp;
    });
  }

  AltaHistorial() {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea registrar Historial?',
      text: "Puedes cancelar!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, registrar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.pacienteService.altaHistorial( this.newhistorial).subscribe( (resp: Pacientes[] | any) => {
          if (resp['resultado'] == 'OK') {
            swalWithBootstrapButtons.fire(
              'Historial Agregado!',
              'Se registro exitosamente.',
              'success'
            )
          }
        });
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancel',
          'No se ha dado de alta el historial!)',
          'error'
        )
      }
    })

  }




  VerExp( idpaciente: number) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea ver el expediente?',
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {

        this.route.navigate(['/expediente', idpaciente]);

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          '',
          'error'
        )
      }
    })

  }

}
