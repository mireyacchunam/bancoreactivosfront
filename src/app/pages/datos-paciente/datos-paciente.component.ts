import { Component, OnInit } from '@angular/core';
import { PacientesService } from '../../services/pacientes.service';
import { Pacientes } from '../../interfaces/pacientes.interface';
import Swal from 'sweetalert2';
import { Text } from '@angular/compiler';


@Component({
  selector: 'app-datos-paciente',
  templateUrl: './datos-paciente.component.html',
  styleUrls: ['./datos-paciente.component.css']
})
export class DatosPacienteComponent implements OnInit {

  //Obtener la lista de pacientes
  pacientes: Pacientes[] | any;
  paciente: any = {};

  p: number = 1;

  filtrarNombre: any = '';

  //paciente: any = {};

  constructor(public PacientesService: PacientesService) { }

  ngOnInit(): void {
    this.obtenerPacientes();
  }

  obtenerPacientes() {
    this.PacientesService.getPaciente().subscribe(resp => {
      this.pacientes = resp;
      console.log(this.pacientes);

    })
  }


  seleccionarPaciente(idpaciente: number) {
    //console.log(idpaciente);
    this.PacientesService.seleccionarPaciente(idpaciente).subscribe((resp:any) => {
      this.paciente = resp[0];
      console.log(this.paciente);
    });
  }

  editarPaciente() {
    this.PacientesService.editarPaciente(this.paciente).subscribe((resp:any)=> {
      if (resp['resultado'] == 'OK') {
        Swal.fire({
          icon: 'success',
          title: 'Paciente modificado correctamente',
          showConfirmButton: false,
          timer: 2000
        })
        this.obtenerPacientes();
      }
    });
  }

  eliminarPaciente(idpaciente: number){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar el paciente?',
      text: "Puedes cancelar!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.PacientesService.eliminarPaciente(idpaciente).subscribe( (resp:any) => {
          if (resp['resultado'] == 'OK') {
            swalWithBootstrapButtons.fire(
              'Registro Eliminado!',
              'Tu registro ha sido eliminado con exito.',
              'success'
            )
            this.obtenerPacientes();
          }
        });
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancel',
          'No se ha eliminado!)',
          'error'
        )
      }
    })
  }


}


